Exceptions
==========

.. autoclass:: pyfygentlescrap.warnings.InvalidParameterWarning

-----

.. autoclass:: pyfygentlescrap.warnings.InvalidRegionWarning

-----

.. autoclass:: pyfygentlescrap.warnings.InvalidResponseWarning

-----

.. autoclass:: pyfygentlescrap.warnings.MarketOpenWarning

-----

.. autoclass:: pyfygentlescrap.warnings.NoInternetWarning

-----

.. autoclass:: pyfygentlescrap.warnings.WrongTypeWarning
