PyFyGentleScrap documentation
=============================

Welcome to PyFyGentleScrap documentation **version :** |version|

**PyFyGentleScrap** is a python package that provide function to scrap financial
data on websites, such as a list of equities for a country or historical EOD values
for a particular ticker.

*Gentle* scrapping means that all web requests are designed to avoid *as much as
possible* the remote servers to detect the requests as scrapping.

All scrapped data are return as `pandas.DataFrame` to easily further compute
statistics, or storing fetched data into a database.

NOTE: **PyFyGentleScrap** package is not affiliated to any website. Please use this
package wisely to avoid to be block.


Examples
========

Example of a basic download of the ticker AAPL, from yahoo.com:

::

    >>> df = pfgs.yahoo_historical_data('MSFT', "2020/01/01", "2020/12/31")
    >>> print(df)
                 open   high    low  ...    volume  dividend  split
    2020-01-02 158.78 160.73 158.33  ...  22622100      0.00   1.00
    2020-01-03 158.32 159.95 158.06  ...  21116200      0.00   1.00
    2020-01-06 157.08 159.10 156.51  ...  20813700      0.00   1.00
    2020-01-07 159.32 159.67 157.32  ...  21634100      0.00   1.00
    2020-01-08 158.93 160.80 157.95  ...  27746500      0.00   1.00
    ...           ...    ...    ...  ...       ...       ...    ...
    2020-12-07 214.37 215.54 212.99  ...  24620000      0.00   1.00
    2020-12-08 213.97 216.95 212.89  ...  23284100      0.00   1.00
    2020-12-09 215.16 215.23 211.21  ...  32440600      0.00   1.00
    2020-12-10 211.77 213.08 210.36  ...  26733300      0.00   1.00
    2020-12-11 210.05 213.32 209.11  ...  30972600      0.00   1.00

    [240 rows x 8 columns]



Table of contents
=================

.. toctree::

   yahoo
   exceptions
   dev


Todo list
=========

.. todo::
    To be written


Indices
=======

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
