For developpers
===============

All contributions are welcome. If you think you've discovered an issue, please read
[this stackoverflow article](https://stackoverflow.com/help/minimal-reproducible-example)
for tips on writing a good bug report.


How to contribute to the code
-----------------------------

1. Forking

::

    git clone https://gitlab.com/your-user-name/pyfygentlescrap.git
    cd pyfygentlescrap
    git remote add upstream https://gitlab.com/your-user-name/pyfygentlescrap


2. Set a virtual environment, and activate it

::

    virtualenv venv_new_folder
    source venv_new_folder/bin/activate


3. Install main and development dependencies:

::

    pip3 install -r requirements.txt
    pip3 install -r requirements-dev.txt


Note: Use the `--upgrade` option can be used at any time to update all package 
dependencies to the last stable version.

4. (optionnal) Run tests to check that everything is working fine:

::

    pytest


5. Create a new branch, test it, check everything is OK, then pull it:

::

    git branch my_super_branch
    git checkout my_super_branch


Code a super functionnality, then test it:

::

    # linting:
    black .
    flake8 .
    # building documention:
    python setup.py build_sphinx
    # building the module:
    python setup.py install
    # testing, coverage:
    python3 -m pytest # or simply `pytest`
    python3 -m coverage run --source=pyfygentlescrap/ -m pytest && python3 -m coverage report -m

If all the above commands works fine, the you can push it to gitlab:

::

    git push


Project files organisation
--------------------------

Folder organisation::

    The project folders are organised:
    pyfygentlescrap
    ├── pyfygentlescrap      -> pyfygentlescrap module
    │ └── ...
    ├── tests                -> all pytest
    │ └── ...
    ├── docs                 -> Documentation
    │ └── ...
    ├── .gitignore
    ├── .gitlab-ci.yml
    ├── LICENSE
    ├── MANIFEST.in
    ├── pytest.ini           -> configuration for pytest
    ├── README.md
    ├── requirements.txt     -> requirements for pyfygentlescrap
    ├── requirements-dev.txt -> requirements for developpers
    ├── setup.cfg            -> configuration for flake8
    └── setup.py             -> building file for setuptools
