Yahoo functions
===============

Historical EOD
--------------

.. autofunction:: pyfygentlescrap.yahoo.yahoo_historical.yahoo_historical_data

-----


Single ticker data
------------------

.. autofunction:: pyfygentlescrap.yahoo.yahoo_ticker.yahoo_ticker

-----


Equity screener
---------------

.. autofunction:: pyfygentlescrap.yahoo.yahoo_screener.yahoo_equity_screener

-----

.. autofunction:: pyfygentlescrap.yahoo.yahoo_screener.nb_equities_available


Yahoo session class
-------------------

.. autoclass:: pyfygentlescrap.yahoo.yahoo_shared.yahoo_session
