# coding: utf-8
# !/usr/bin/python3

import datetime
import pandas
import pytest
import pyfygentlescrap as pfgs
from pyfygentlescrap import (
    InvalidParameterWarning,
    # InvalidRegionWarning,
    # InvalidResponseWarning,
    # WrongTypeWarning,
)


@pytest.mark.parametrize("region", ["United States", "France"])
@pytest.mark.parametrize("year", [datetime.date.today().year, 2000, 1970])
def test_economic_events_exist_for_valid_years(region, year):
    result = pfgs.nb_events_available(
        region=region,
        from_date=f"{year}-01-01",
        to_date=f"{year}-01-31",
    )
    assert len(result) > 0


@pytest.mark.parametrize(
    "from_date, to_date",
    [
        ("0000-01-01", "0000-12-31"),
        ("1492-01-01", "1492-12-31"),
        ("not_a_valid_date", "2021-01-01"),
        (True, False),
    ],
)
def test_economic_no_event_exists_for_invalid_years(from_date, to_date):
    with pytest.warns(InvalidParameterWarning):
        result = pfgs.nb_events_available(
            region="United States",
            from_date=from_date,
            to_date=to_date,
        )
    assert len(result) == 0


@pytest.mark.parametrize(
    "date, event_type, expected",
    [
        ("2021-05-24", "earningscount", 103),
        ("2021-01-04", "splitscount", 24),
        ("2021-01-05", "ipoinfocount", 2),
    ],
)
def test_precise_economic_event(date, event_type, expected):
    from_date = pandas.to_datetime(date).date()
    to_date = from_date + pandas.DateOffset(1)
    result = pfgs.nb_events_available(
        region="United States", from_date=date, to_date=to_date
    )
    result = result.loc[from_date, event_type]
    assert result == expected
